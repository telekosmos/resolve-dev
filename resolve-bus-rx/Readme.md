reSolve adapter for (very) simple Rx bus
===============

Very simple adapter to figure out and experiment with [reSolve event store](https://github.com/reimagined/resolve/tree/master/packages/resolve-es) approach, which is used by [resolve-command](https://github.com/reimagined/resolve/blob/master/packages/resolve-command/README.md), a sort of implementation of the C -command- for CQRS paradigm.

As by specification, an adapter for resolve buses are just an object with methods `subscribe` and `publish`. The bus "connection" or access is implemented in the function body and called just once, returning a promise. All methods return a promise to validate/simultate asynchrony. 

This is intended to use as a bus for resolve-es just like:

```javascript
// storage adapter
const createLowdbStorageAdapter = require('../resolve-storage-lowdb/src');
// or could be somethin like
// const createMemAdapter = require('resolve-storage-mem') // * fictitious

// bus adapter
const createRxBusAdapter = require('../resolve-bus-rx/src');

const topicName = 'myFirstTopic';
const createEventStore = require('resolve-es');

const eventStore = createEventStore({
	storage: createLowdbStorageAdapter(topicName),
	bus: createRxBusAdapter({topicName: topicName})
});
```

## Test
From the repo root folder and using local mocha installation:
```bash
./node_modules/mocha/bin/_mocha resolve-bus-rx/test/*.test.js
# mocha resolve-bus-rx/test/*.test.js # if mocha globally installed
```

## Upgrading bus adapter to store messages (kafka-like)
Use simple lowdb to emulate the storage system by pushing up events into a (very) simple store (json file). A lowdb storage adapter for reSolve is implemented.

## Simulating kafka 
__[TODO]__ In order to use kafka as event store, both storage and bus adapter has to target same backend system, differently what has been done til now, where [resolve-es](https://github.com/reimagined/resolve/tree/master/packages/resolve-es) uses rabbitmq, rx for bus and independent storage (file, mongo, ...).

With kafka approach, producers and consumers get into stage.
There still have to be adapters for resolve-es though.

### resolve-kafka-storage adapter
__[TODO]__ Based on the kafka topic concept to store the events in different topics. Save and load events to/from topics.

### resolve-kafka-bus adapter
__[TODO]__ Subscribe to topics to be notified on events.