'use strict';

const Promise = require('bluebird');
const sinon = require('sinon');
const chai = require('chai');
const should = chai.should();
const chaiAsPromised = require('chai-as-promised');
const sinonChai = require('sinon-chai');

const rxBus = require('../../rx-bus/src');
const adapter = require('../src');

chai.use(chaiAsPromised);
chai.use(sinonChai);

const fakeEmitter = {
	emit: () => {},
	addListener: () => {},
	removeListener: () => {}
};
const adapterCfg = {
	topicName: 't1'
};

describe('Rx Bus', () => {
	let rxBusMock, stubEmitter, mockTopic, rxBusStub, fakeTopic;
	const event1 = {
		type: 'ONE',
		payload: {
			data: 'AAA'
		}
	};
	const event2 = {
		type: 'TWO',
		payload: {
			data: 'BBB'
		}
	};

	beforeEach(() => {
		// Mock rxBus: assertTopic, consume, publish
		stubEmitter = sinon.stub(fakeEmitter, 'emit').returns(true);

		fakeTopic = {
			id: 1,
			name: 't1',
			topic: fakeEmitter
		};
		// const mockTopic = {id: 1, name: 't1', emitter: mockEmitter};
		rxBusMock = sinon.mock(rxBus);
	});

	afterEach(() => {
		stubEmitter.restore();
		rxBusMock.restore();
	});

		describe('publish', () => {

		describe('previously get a topic to publish events', () => {
			it('default', () => {
				const assertMock = rxBusMock.expects('assertTopic')
					.once()
					.withArgs({
						name: 'default'
					})
					.returns(fakeTopic);

				const busInstance = adapter({});
				rxBusMock.verify();

				/* const assertMock = rxBusMock.expects('assertTopic')
					.twice()
					.withArgs({name: 'default'})
					.returns(fakeTopic); 

				return busInstance.publish({}) // assertTopic is called here
					.then(() => busInstance.publish())
					.then(() => {
						// assertMock.should.have.been.calledOnce;
						// assertMock.should.have.been.calledWith({name: 'default'});
						rxBusMock.verify()
					});
					*/
			});

			it('named', () => {
				const topicName = 't1';
				rxBusMock.expects('assertTopic')
					.once()
					.withArgs({
						name: topicName
					});
				// .withArgs(adapterCfg)

				const busInstance = adapter(adapterCfg); // adapter({topicName});
				rxBusMock.verify();
				/*
				return busInstance.publish({})
					.then(() => rxBusMock.verify())
				*/
			});

			it('works the same way for different import types', () => {
				adapter.should.be.equal(require('../src'));
			});
		});


		it('one event', () => {
			// Should test assertTopic is called, then publish => emitter emit event
			rxBusMock.expects('assertTopic').once().returns(fakeTopic);
			rxBusMock.expects('_findTopic').withArgs(fakeTopic).returns(fakeTopic);
			rxBusMock.expects('publish').withArgs(fakeTopic, event1).callThrough();

			const busInstance = adapter(adapterCfg); // return {publish, subscribe}
			return busInstance.publish(event1)
				.then(() => {
					stubEmitter.should.have.been.calledOnce;
					rxBusMock.verify();
				});
		});

		describe('multiple events', () => {
			beforeEach(() => {
				rxBusMock.expects('assertTopic').once().returns(fakeTopic);
				rxBusMock.expects('_findTopic').returns(fakeTopic).atMost(2); // without atMost(2)/atLeast(2) fails :-S
				rxBusMock.expects('publish').withArgs(fakeTopic, event1).callThrough().onFirstCall();
				rxBusMock.expects('publish').withArgs(fakeTopic, event2).callThrough().onSecondCall();
			});

			it('multiple events', () => {
				const busInstance = adapter(adapterCfg); // return {publish, subscribe}

				return busInstance.publish(event1)
					.then(() => busInstance.publish(event2))
					.then(() => {
						stubEmitter.should.have.been.calledTwice; // one for each publish
						stubEmitter.firstCall.should.have.been.calledWith(fakeTopic.name, event1);
						rxBusMock.verify();
					});

				// triggerSpy.callCount.should.be.equal(2);
				// triggerSpy.args[0][0].should.be.deep.equal(event1);
				// triggerSpy.args[1][0].should.be.deep.equal(event2);
			});

			it('multiple events in parallel', () => {
				const busInstance = adapter(adapterCfg); // return {publish, subscribe}

				Promise.all([
						busInstance.publish(event1),
						busInstance.publish(event2)
					])
					.then((results) => {
						should.exist(results);
						results.should.not.be.empty;
						results.length.should.be.equal(2);
						stubEmitter.should.have.been.calledTwice;
						rxBusMock.verify();
					})
			});
		})

	});

	describe('subscribe', () => {
		beforeEach(() => {
			// rxBusMock.expects('publish').callThrough();
			rxBusMock.expects('assertTopic').once().returns(fakeTopic);
		});


		it('should register for a topic events', () => {
			// stubEmitter.restore();
			let eventHandlerSpy = sinon.spy();

			rxBusMock.expects('consume').callsFake((topic, fnHandler) => {
				// Here the parameters have to be asserted
				topic.should.be.deep.equal(fakeTopic);
				fnHandler.should.be.a('function');
				fnHandler.should.be.equal(eventHandlerSpy);
				// console.log('Brrrrrrrrrrr: '+JSON.stringify(topic), fnHandler);
			});
			const adapterInstance = adapter(adapterCfg); // adapter for a topic in the bus/message system
			// subscribe -> rxBus.consume(topic, handler) -> kefir.fromEvents(emitter, topic).onValue(handler)
			adapterInstance.subscribe(eventHandlerSpy)
				.then(() => rxBusMock.verify());

			// eventHandlerSpy.lastCall.args[0].should.be.deep.equal(event1);
		});

		it('should respond to published events (consume) after subscribing', () => {
			let eventHandlerSpy = sinon.spy();
			// Mock the rx bus in order to below things work
			// rxBusMock.expects('publish').withArgs(fakeTopic, event1).callThrough(); // emit is faked
			rxBusMock.expects('publish').callsFake((topic, ev) => {
				ev.should.be.deep.equal(event1);
				topic.should.be.equal(fakeTopic);
				eventHandlerSpy(ev);
			});
			// rxBusMock.expects('consume').withArgs(fakeTopic, eventHandlerSpy).callThrough();
			rxBusMock.expects('consume').callsFake((topic, handler) => {
				topic.should.be.deep.equal(fakeTopic);
				handler.should.be.a('function');
			});

			const adapterInstance = adapter(adapterCfg);
			adapterInstance.subscribe(eventHandlerSpy)
				.then(() => adapterInstance.publish(event1))
				.then(() => {
					eventHandlerSpy.should.have.been.called;
					eventHandlerSpy.should.have.been.calledWith(event1);
					rxBusMock.verify();
				})
		});

		it('with multiple consumers', () => {
			const evHandlerSpy1 = sinon.spy(),
				evHandlerSpy2 = sinon.spy();

			rxBusMock.expects('publish').callsFake((topic, ev) => {
				topic.should.be.deep.equal(fakeTopic);
				ev.should.be.deep.equal(event1);
				evHandlerSpy1(ev);
				evHandlerSpy2(ev);

				return true; // fake the emitter.emit method return value
			});

			rxBusMock.expects('consume').onFirstCall().callsFake((topic, handler) => {
				topic.should.be.deep.equal(fakeTopic);
				handler.should.be.equal(evHandlerSpy1);
			});
			rxBusMock.expects('consume').onSecondCall().callsFake((topic, handler) => {
				topic.should.be.deep.equal(fakeTopic);
				handler.should.be.equal(evHandlerSpy2);
			})

			// Test
			const adapterInstance = adapter(adapterCfg);
			Promise.all([
					adapterInstance.subscribe(evHandlerSpy1),
					adapterInstance.subscribe(evHandlerSpy2)
				])
				.then(() => adapterInstance.publish(event1))
				.then((result) => {
					result.should.be.true;
					evHandlerSpy1.should.have.been.calledWith(event1);
					evHandlerSpy2.should.have.been.calledWith(event1);
					rxBusMock.verify();
				})
		});
	

		it ('multiple events in parallel', () => {
			rxBusMock.restore(); // no mocks here :-/
			
			const adapterInstance = adapter(adapterCfg);
			const eventHandlerSpy = sinon.spy();
			adapterInstance.subscribe(eventHandlerSpy)
				.then(() => Promise.all([adapterInstance.publish(event1),  adapterInstance.publish(event2)]))
				.then(() => {
					eventHandlerSpy.should.have.been.calledTwice;
					eventHandlerSpy.firstCall.args[0].should.be.deep.equal(event1);
				});
		});

		xit('with multiple events in parallel', () => {
			const eventHandlerSpy = sinon.spy();
			/*
			let publishStub = rxBusMock.expects('publish');
			
			rxBusMock.expects('publish').withArgs(fakeTopic, event1).callsFake((topic, ev) => {
				console.log(JSON.stringify(ev));
				topic.should.be.deep.equal(fakeTopic);
				ev.should.be.deep.equal(event1);
				eventHandlerSpy(ev);

				return true; // fake the emitter.emit method return value
			});

			// should be rxBusMock.expects('publish').onSecondCall().callsFake((topic, ev) => ...)
			rxBusMock.expects('publish').withArgs(fakeTopic, event2).callsFake((topic, ev) => {
				console.log(JSON.stringify(ev));
				topic.should.be.deep.equal(fakeTopic);
				ev.should.be.deep.equal(event2);
				eventHandlerSpy(ev);

				return true; // fake the emitter.emit method return value
			});
			*/			
			rxBusMock.expects('_findTopic').withArgs(fakeTopic).returns(fakeTopic);
			rxBusMock.expects('publish').callThrough().atLeast(2);
			rxBusMock.expects('consume').callsFake((topic, handler) => {
				topic.should.be.deep.equal(fakeTopic);
				handler.should.be.equal(eventHandlerSpy);
			});

			// Test
			const adapterInstance = adapter(adapterCfg);
			/*
			const promises = Promise.all([
				adapterInstance.publish(event1),
				adapterInstance.publish(event2)
			]);
			*/
			adapterInstance.subscribe(eventHandlerSpy)
				.then(() => adapterInstance.publish(event1))
				.then(() => adapterInstance.publish(event2))
				.then((results) => {
					should.exist(results);
					// results.length.should.be.equal(2);
					eventHandlerSpy.should.have.been.calledTwice;
					eventHandlerSpy.firstCall.args[0].should.be.deep.equal(event1);
					rxBusMock.verify();
				})
				/*
				.catch((err) => {
					console.error(err.message);
				}); */
		});

	})

});