'use strict';

const rxBus = require('../../rx-bus/src');
const Promise = require('bluebird');

/**
 * Creates a resolve rx-bus adapter for a topic passed in as an option
 * @param {object} options 
 */
function createAdapter(options) {
	// let handler = () => {};
	// Need some code to be called from the methods to initialize the bus access
	/**
	 * Initialize a new topic (channel in rabbitmq jargon) in the bus. 
	 * Inherently asynchronous (here simulating)
	 * @param {Promise} opts and object with at least a topicName value 
	 */
	const initBus = function(opts) {
		return new Promise((resolve, reject) => {
			resolve(rxBus.assertTopic({name: opts.topicName || 'default'}));
		}) 
	};

	const promiseBus = initBus(options);
	return {
			subscribe: callback => promiseBus.then(topic => rxBus.consume(topic, callback)),
			// publish: event => initBus(options).then(topic => rxBus.publish(topic, event)) // in this way, called iniBus everytime
			publish: event => promiseBus.then(topic => rxBus.publish(topic, event))
	};
}

module.exports = createAdapter;
