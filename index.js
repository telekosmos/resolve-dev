'use strict';
/*
import createEventStore from 'resolve-es'
import createInFileStorageAdapter from 'resolve-storage-lite'
import createInMemoryBusAdapter from 'resolve-bus-memory'
*/

const createEventStore = require('resolve-es');

const createInFileStorageAdapter = require('resolve-storage-lite');
const createLowdbStorageAdapter = require('./resolve-storage-lowdb/src');

const createInMemoryBusAdapter = require('resolve-bus-memory');
const createRxBusAdapter = require('./resolve-bus-rx/src');

const topicName = 'myFirstTopic';

const eventStore = createEventStore({
	// storage: createInFileStorageAdapter({ pathToFile: './event-store.db' }),
	storage: createLowdbStorageAdapter(topicName),
	// bus: createInMemoryBusAdapter()
	bus: createRxBusAdapter({topicName: topicName})
});

eventStore.subscribeByEventType(['UserCreated'], event =>
  console.log('1. [subscribe ByEventType]\nFiltered by event type', event)
);
eventStore.subscribeByEventType(['UserUpdated'], event =>
	console.log('1. [subscribe ByEventType (UserUpdated)]\nFiltered by event type', event)
);

/*
eventStore.subscribeByAggregateId(['1', '2'], event =>
  console.log('2. [subscribeByAggregateId], filtered by aggregate id', event)
)
*/
eventStore.getEventsByAggregateId('1', event =>
  console.log('3.[get EventsByAggregateId]\nAggregate event loaded', event)
)
/*
eventStore.subscribeByEventType(['UserCreated'], event =>
  console.log('4. [subscribeByEventType]Fresh event emitted from bus by event type', event),
true)
*/
eventStore.subscribeByAggregateId(['1', '2'], event =>
  console.log('5. [subscribe ByAggregateId]\nFresh event emitted from bus by aggregate id', event),
true)

const events = [{
  aggregateId: '1',
  type: 'UserCreated',
  payload: {
    email: 'test1@user.com'
  }
}, {
  aggregateId: '2',
  type: 'UserCreated',
  payload: {
    email: 'test2@user.com'
  }
}, {
  aggregateId: '3',
  type: 'UserCreated',
  payload: {
    email: 'test3@user.com'
  }
}/*, {
  aggregateId: '3',
  type: 'UserCreated',
  payload: {
    email: 'test34@user.com'
  }
}, {
  aggregateId: '4',
  type: 'UserUpdated',
  payload: {
    email: 'test4@user.com'
  }
}*/];

let count = 0;
console.log('Starting interval for MEM!!!');
const intervalId = setInterval(() => {
	console.log(`\n[EVENT] aggId: ${events[count].aggregateId} - type: ${events[count].type}`);
	eventStore.saveEvent(events[count++]);
	if (count === events.length)
		clearInterval(intervalId);

}, 1250);
/*
events.forEach((event) => {
	eventStore.saveEvent(event)
});
*/