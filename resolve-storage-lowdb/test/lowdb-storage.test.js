'use strict';


const Promise = require('bluebird');
const sinon = require('sinon');
const chai = require('chai');
const should = chai.should();
const chaiAsPromised = require('chai-as-promised');
const sinonChai = require('sinon-chai');
const proxyquire = require('proxyquire');

chai.use(chaiAsPromised);
chai.use(sinonChai);


describe('lowdb-storage', () => {
		let ldStorage, lowdbStub, fileAsyncStub;
		const evData = {id: 1, aggId: 6, type: 'red', payload: {}},
					evData2 = {id: 2, aggId: 5, type: 'red', payload: {}};

    beforeEach(() => {
        lowdbStub = sinon.stub().returns({});
        fileAsyncStub = sinon.stub().returns({
            read: sinon.spy(),
            write: sinon.spy()
        });
        ldStorage = proxyquire('../src/lowdb-storage.js', {
            'lowdb': lowdbStub,
            'lowdb/adapters/FileAsync': fileAsyncStub
        });
    });

    afterEach(() => {});

    describe('storage creation', () => {
        it('should create a db instance', () => {
            const filename = "topic.json";
            let db = ldStorage.conn();

						// db.should.eventually.be.fulfilled;
						// should.exist(db);
            fileAsyncStub.should.have.been.calledWith;
            lowdbStub.should.have.been.calledOnce;
        });

        it('should create a topic/collection', (done) => {
            const topicName = "t0";
            let valueStub = sinon.stub(),
								writeStub = sinon.stub(),
								setStub = sinon.stub();
            valueStub.onFirstCall().returns(false);
            
            let dbMock = {
                set: setStub.returns({
                    write: writeStub
                }),
                has: sinon.stub().returns({
                    value: valueStub
                }),
                write: writeStub,
                value: valueStub
            };
						writeStub.onFirstCall().resolves();

            let dbPromise = ldStorage._assertTopic(dbMock, topicName);
            // assert things were called
            dbPromise.then((result) => {
								should.exist(result);
								result.should.be.equal(dbMock);
								dbMock.has.should.have.been.calledWith(topicName);
								valueStub.should.have.been.calledOnce;
								setStub.should.have.been.calledWith(topicName, []);
								writeStub.should.have.been.calledOnce;
                done();
            });
        });


        it('should not create a existent topic', (done) => {
            const filename = "filename.json",
                topicName = "t0";
            let valueStub = sinon.stub(),
								writeStub = sinon.stub(),
								setStub = sinon.stub();
            valueStub.onFirstCall().returns(false);
            valueStub.onSecondCall().returns(true);
            writeStub.onFirstCall().resolves();
            let dbMock = {
                set: setStub.returns({
                    write: writeStub
                }),
                has: sinon.stub().returns({
                    value: valueStub
                }),
                write: writeStub,
                value: valueStub
            };

            let dbPromise = ldStorage._assertTopic(dbMock, topicName);
            // assert things were called
            dbPromise
                .then(dbObj => ldStorage._assertTopic(dbObj, topicName))
                .then((result) => {
                    should.exist(result);
                    // result.should.be.equal(dbMock);
                    dbMock.has.should.have.been.calledTwice;
                    dbMock.has.firstCall.should.be.calledWith(topicName);
                    dbMock.has.secondCall.should.be.calledWith(topicName);
                    dbMock.value.should.have.been.calledTwice;
										dbMock.set.should.have.been.calledOnce;
										dbMock.set.should.have.been.calledWith(topicName, []);
                    dbMock.write.should.have.been.calledOnce;
                    done();
                });
        });
    });

    describe('write/save', () => {
        let valueStub, writeStub, getStub, pushStub, lastStub, dbMock;
				const topic = 'topicName';

        beforeEach(() => {
            valueStub = sinon.stub();
            writeStub = sinon.stub();
            getStub = sinon.stub();
            pushStub = sinon.stub();
            valueStub.returns(true);
						writeStub.onFirstCall().resolves(evData);
						writeStub.onSecondCall().resolves(evData2);
						lastStub = sinon.stub().onFirstCall().returns(evData);
						lastStub.onSecondCall().returns(evData2);
            
            dbMock = {
                get: getStub,
                set: sinon.stub().returns({
                    write: writeStub
                }),
                has: sinon.stub().returns({
                    value: valueStub
                }),
                write: writeStub,
                value: valueStub,
								push: pushStub,
								last: lastStub
            };
            pushStub.returns(dbMock);
            getStub.withArgs(topic).returns(dbMock);
        });

        afterEach(() => {
            valueStub.reset();
            writeStub.reset();
            getStub.reset();
            pushStub.reset();
        });


        it('should save an event', (done) => {
            let savePromise = ldStorage.saveEvent(dbMock, {topic: topic, payload: evData});

						savePromise.then(result => {
							savePromise.should.be.fulfilled;
							savePromise.should.eventually.be.equal(dbMock); // mock result!!
							dbMock.has.should.have.been.calledWith(topic);
							dbMock.get.should.have.been.called;
							dbMock.get.should.have.been.calledWith(topic);
							dbMock.push.should.have.been.calledWith(evData);
							writeStub.should.have.been.calledOnce;
							
							done();
						})
						
        });

        
				it('should save multiple events in parallel', (done) => {
					let results = Promise.all([
						ldStorage.saveEvent(dbMock, {topic: topic, payload: evData}),
						ldStorage.saveEvent(dbMock, {topic: topic, payload: evData})
					]);

					results.then(res => {
						results.should.be.fulfilled;
						results.should.eventually.be.an('array');
						results.should.eventually.have.lengthOf(2);
						results.should.eventually.have.ordered.members([dbMock, dbMock]);
						
						writeStub.should.have.been.calledTwice;
						getStub.should.have.been.calledTwice;

						done();
					})
				});
		});
		
		describe('read/load', () => {
			let cbStub, queryStub, valueStub, filterStub, getStub, dbMock;
			const values = [evData, evData2], topicName = 'topicName';
			beforeEach(() => {
				cbStub = sinon.stub();
				queryStub = sinon.stub();
				getStub = sinon.stub();
				filterStub = sinon.stub();
				valueStub = sinon.stub().returns(values);

				dbMock = {
					get: getStub,
					filter: filterStub.returns({
						value: valueStub
					}),
					value: valueStub
				};
				getStub.returns(dbMock);
			});

			afterEach(() => {
			});

			it('runs a callback for each found item', () => {
				const promiseLoad = ldStorage.loadEvents(dbMock, {topic: topicName, query: queryStub}, cbStub);

				promiseLoad.should.be.fulfilled;
				promiseLoad.should.eventually.be.equal(values);
				dbMock.get.should.have.been.calledOnce;
				dbMock.filter.should.have.been.calledOnce;
				cbStub.should.have.been.calledTwice;
				cbStub.firstCall.should.have.been.calledWith(evData);
				cbStub.secondCall.should.have.been.calledWith(evData2);
			});

		});
});