'use strict';

// const Promise = require('bluebird');
const sinon = require('sinon');
const chai = require('chai');
const should = chai.should();
const chaiAsPromised = require('chai-as-promised');
const sinonChai = require('sinon-chai');

const storage = require('../src/lowdb-storage');
const adapter = require('../src/index');

chai.use(chaiAsPromised);
chai.use(sinonChai);


describe('lowdb storage adapter', () => {

	let sandbox, dbSpy;

	const topic = 'some-topic';

	beforeEach(() => {
			sandbox = sinon.sandbox.create();

			dbSpy = sinon.spy();
			sandbox.stub(storage, 'conn').resolves(); // returns(Promise.resolve());
			sandbox.stub(storage, 'saveEvent');
			sandbox.stub(storage, 'loadEvents');
	});

	afterEach(() => {
			sandbox.restore();
	});

	it('saveEvent should save event to storage', (done) => {
			const event = { type: 'SOME_EVENT' };

			const savePromise = adapter(topic).saveEvent(event);

			savePromise.then(result => {
				savePromise.should.be.fulfilled;
				storage.conn.should.have.been.called;
				storage.saveEvent.should.have.been.calledOnce;
				done();
			})
	});

	it('loadEventsByTypes should load events by types from storage', (done) => {
			const types = ['SOME_EVENT_ONE', 'SOME_EVENT_TWO'];
			const callbackSpy = sinon.spy();

			const loadPromise = adapter(topic).loadEventsByTypes(types, callbackSpy);
			loadPromise.then(result => {
				loadPromise.should.be.fulfilled;
				storage.conn.should.have.been.calledOnce;
				storage.loadEvents.should.have.been.calledOnce;
				// storage.loadEvents.should.have.been.calledWith(dbSpy, it => types.include(it.type), callbackSpy)
				storage.loadEvents.firstCall.args.should.be.an('array').that.have.lengthOf(3);
				storage.loadEvents.firstCall.args[1].should.be.a('function');
				done();
			});
	});

	it('loadEventsByAggregateIds should load events by aggregateId from storage', (done) => {
			const ids = ['id1', 'id2', 'id3'];
			const callback = sinon.spy();

			const loadPromise = adapter(topic).loadEventsByAggregateIds(ids, callback);
			loadPromise.then(result => {
				loadPromise.should.be.fulfilled;
				storage.conn.should.have.been.calledOnce;
				storage.loadEvents.should.have.been.calledOnce;
				// storage.loadEvents.should.have.been.calledWith(dbSpy, it => types.include(it.type), callbackSpy)
				storage.loadEvents.firstCall.args.should.be.an('array').that.have.lengthOf(3);
				storage.loadEvents.firstCall.args[1].should.be.a('function');
				storage.loadEvents.firstCall.args[2].should.be.equal(callback);
				done();
			});
			// sinon.assert.calledWith(storage.prepare, topic);
			// sinon.assert.calledWith(storage.loadEvents, { aggregateId: { $in: ids } }, callback);
	});
	
});