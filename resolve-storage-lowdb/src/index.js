'use strict';

const storage = require('./lowdb-storage');

module.exports = function(topicName) {
	const conn = storage.conn();

	return {
			saveEvent: event => conn.then(db => storage.saveEvent(db, {topic: topicName, payload: event})),
			loadEventsByTypes: (types, callback) =>
					conn.then(db => storage.loadEvents(db, it => types.includes(it.type), callback)),
					// prepareStorage.then(db => new Promise((resolve) => db.findOne(...);));
			loadEventsByAggregateIds: (ids, callback) =>
					conn.then(db => storage.loadEvents(db, it => ids.includes(it.aggregateId), callback))
	};
}