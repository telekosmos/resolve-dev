'use strict';

const lowdb = require('lowdb');
const FileAsync = require('lowdb/adapters/FileAsync');

process.on('unhandledRejection', (reason, p) => {
  console.log('Unhandled Rejection at: Promise', p, 'reason:', reason.stack);
  // application specific logging, throwing an error, or other logic here
});

const DB_NAME = 'event-store.json';
let lowdbStorage = {

	conn: function() {
		const adapter = new FileAsync(DB_NAME);
		return lowdb(adapter);
	},

	_assertTopic: function(db, topic) {
		if (db.has(topic).value())
			return new Promise((res) => res(db));
		else
			return db.set(topic, [])
				.write()
				.then(() => db);
	},

	saveEvent: function(db, data) {
		const topic = data.topic || 'default';
			
		return lowdbStorage._assertTopic(db, topic)
			.then(db => db.get(topic).push(data.payload).write()) //
			.then(() => db) // returns promise
	},


	/**
	 * Returns a promise resolve with the found values
	 * @param {Object} db
	 * @param {Function} query a function to filter objects in an array
	 * @param {Function} cb
	 * @returns {Promise} resolves to found values
	 */
	loadEvents: function(db, query, cb) {
		return new Promise((res, rej) => {
			const values = db.get(query.topic)
				.filter(query.query)
				.value() // returns value

			if (cb)
				values.forEach(cb);

			res(values);
		}); 
	}
}

module.exports = lowdbStorage;