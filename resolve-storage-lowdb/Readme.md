resolve-storage-lowdb
=======================

Very simple [resolve-es](https://github.com/reimagined/resolve/tree/master/packages/resolve-es) storage adapter based on [lowdb](http://typicode.github.io/lowdb/) and trying to mimic the kafka topic-based storage.

Following the specification to implement adaptors, this is just an object with three methods: `saveEvents`, `loadEventsByType` and `loadEventsByAggregateIds`. A wrapper around `lowdb` (`src/lowdb-storage.js`) helps to implement the required methods.

E.g.
```javascript
const storage = require('src/lowdb-storage');

const conn = storage.conn() // return a promise which resolves to a lowdb instance
conn.then(db => storage.saveEvent(db, {topic: topicName, payload: event}))
//...
// here query is a lodash predicate (includes, finde, has, ...)
conn.then(db => storage.loadEvent(db, query, cb)) 
```

Mind this implementation is intended to take part of the resolve storage adapter (`src/index.js`). 

## Test
From the repo root folder and using local mocha installation:
```bash
./node_modules/mocha/bin/_mocha resolve-storage-db/test/*.test.js
# mocha resolve-storage-db/test/*.test.js # if mocha globally installed
```