
reSolve-es simple adapters example & tests
==========================

Simple rx bus adapter (using [kefir](https://kefirjs.github.io/kefir/) just for fun and support) and storage adapter (using [lowdb](http://typicode.github.io/lowdb/) for similar) to use with [resolve-es](https://github.com/reimagined/resolve/tree/master/packages/resolve-es). Purpose is better understanding in order to implemente further, more complex, adapters (especially targeting [kafka](http://kafka.apache.org) as bus and store).

## Example
These adapters can be used with `resolve-es` just like:
```javascript
// index.js
// ...
// storage adapter
const createLowdbStorageAdapter = require('../resolve-storage-lowdb/src');

// bus adapter
const createRxBusAdapter = require('../resolve-bus-rx/src');

const topicName = 'myFirstTopic';
const createEventStore = require('resolve-es');

const eventStore = createEventStore({
	storage: createLowdbStorageAdapter(topicName),
	bus: createRxBusAdapter({topicName: topicName})
});

// ... 
// custom logic with event store
```

## Install & Test
The repo is actually based on a simple use example of resolve-es, and the packages are in folders. It's easy to pull them out to independent repos.

#### Install
```bash
git clone -b dev https://telekosmos@bitbucket.org/telekosmos/resolve-dev.git
cd resolve-dev
npm install
```

In order to run the tests for the packages:
```bash
npm test
```
or, individually:

```bash
npm run test-rxbus
npm run test-storage
npm run test-resolve-bus
```

To run a simple example on using resolve event store, just
```bash
npm install && node index.js
```
