rx-bus
======

Try to simulate a bus by using a rx (reactive) model.

Singleton implementation of a message bus, topic based.

Microsample:

```javascript
const rxBus = require('rx-bus/src');

const topic = rxbus.assertTopic({name: 'myTopicName'}) // returns a promise

// consume from a topic
rxBus.consume(topic, cb);

// publish into a topic
rxBus.publish(topic, event);

```

It emits an event using the node EventEmitter. The payload is the data coming from client. Of course an API gateway should be in between.

As a bus, clients (subscribers) have to be able to subscribe to some events
(check rabbitmq/ampqlib node implementation -and kafka-node- in order to see whether or not subscribe to some events -> Topics shouldn't allow that). In this way, the reference used for implementaion was kind of similar to amqplib

This is mostly intended as an implementation addressed to help to implement a very simple reactive bus adapter for [reSolve](https://github.com/reimagined/resolve).

## Test
From the repo root folder and using local mocha installation:
```bash
./node_modules/mocha/bin/_mocha rx-bus/test/*.test.js
# mocha rx-bus/test/*.test.js # if mocha globally installed
```