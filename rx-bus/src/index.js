'use strict';

const UUID = require('uuid4');
const Kefir = require('kefir');
const EventEmitter = require('events');

class RxBus {
	constructor() {
		this.topics = [];
		this.id = UUID();
	}

	_findTopic(topic) {
		return this.topics.length === 0
			? undefined 
			: this.topics.find(t => t == topic) 
					|| this.topics.find(t => t.id === topic.id)
					|| this.topics.find(t => t.name === topic.name);
	}

	/**
	 * 
	 * @param {object} idObj an object with right properties to get/create an object
	 */
	assertTopic(idObj) {
		if (typeof idObj !== 'object')
			return undefined;
		
		let topic = idObj.id 
			? this.topics.find(t => t.id === idObj.id)
			: this.topics.find(t => t.name === idObj.name);

		if (!topic) {
			topic = {
				name: idObj.name || 'default-'+this.topics.length+1, 
				id: this.topics.length+1,
				topic: new EventEmitter()
			};
			this.topics.push(topic);
		}
		return topic;
	}

	/**
	 * Publish an event to a topic by emitting
	 * @param {object} topic an object with some topic property 
	 * @param {object} payload data to publish
	 * @returns {boolean} the return value of the EventEmitter.emit method
	 */
	publish(topic, payload) {
		let t = this._findTopic(topic);
		return t.topic.emit(t.name, payload);
	}

	/**
	 * Consume the events emitted by topics
	 * @param {object} topic an object with some topic property 
	 * @param {function} handler
	 */
	consume(topic, handler) {
		let t = this._findTopic(topic);
		return Kefir.fromEvents(t.topic, t.name).onValue(handler);
	}
}

let Bus = new RxBus();
module.exports = Bus;