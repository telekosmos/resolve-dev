'use strict';

const Promise = require('bluebird');
const sinon = require('sinon');
const chai = require('chai');
const should = chai.should();
const chaiAsPromised = require('chai-as-promised');
const sinonChai = require('sinon-chai');

chai.use(chaiAsPromised);
chai.use(sinonChai);

let rxBus = require('../src');

/**
 * Reactive (simulation) bus. 
 */
describe('Rx (Fake) Bus', () => {

	xdescribe('a trivial test', () => {
		let testVar, asyncFn;
		beforeEach(() => {
			testVar = true;
		});

		it('should be always truhty', () => {
			should.exist(testVar);
			testVar.should.be.true;
		});

		it('should be eventually false', () => {
			let p = new Promise((resolve) => {
				resolve(testVar);
			});

			p.should.eventually.be.false;
		})
	});

	describe('connection', () => {
		let rxBus, id;
		beforeEach(() => {
			rxBus = require('../src');
			id = rxBus.id;
		});

		afterEach(() => {
			rxBus.topics.length = 0;
		});
		it('should return an instance of bus', () => {
			should.exist(rxBus);
		});

		it('should return the same instance of bus (singleton)', () => {
			let rxBus2 = require('../src');
			should.exist(rxBus2);
			rxBus.should.be.eql(rxBus2);
			rxBus2.id.should.be.equal(id);
			rxBus2.id.should.be.equal(rxBus.id);
		});
	})

	// utils methods
	describe('utils', () => {
		before(() => {
			rxBus = require('../src');
			rxBus.assertTopic({name: 'topicOne'});
		});

		after(() => {
			rxBus.topics.length = 0;
		});

		describe('_findTopic', () => {
			it('should get the topic by id', () => {
				let id = 1;
				let topic = rxBus._findTopic({id: id});
				should.exist(topic);
				topic.should.have.property('id', 1);
				topic.should.have.property('name');
			});

			it('should get the topic by name', () => {
				let name = 'topicOne';
				let topic = rxBus._findTopic({name: name});
				should.exist(topic);
				topic.should.have.property('name', name);
			});

			it('should return undefined on non existant', () => {
				const name = 'foo', id = 2;
				let topic = rxBus._findTopic({name: name});
				should.not.exist(topic);
				
				topic = rxBus._findTopic({id: id});
				should.not.exist(topic);
			});

			it('should return undefined on wrong params', () => {
				let topic = rxBus._findTopic(1);
				should.not.exist(topic);
			});

			it('should return undefined if no topics', () => {
				rxBus.topics.length = 0;
				let topic = rxBus._findTopic({name: 'foot'});
				let topic2 = rxBus._findTopic({id: 1});
				should.not.exist(topic);
				should.not.exist(topic2);
			});
		})
	});


	describe('topics', () => {
		let rxBus, topicName = '1st';
		before(() => {
			rxBus = require('../src');
		});

		after(() => {
			rxBus.topics.length = 0;
		});

		it('should empty list if no topics', () => {
			rxBus.topics.should.be.empty;
		});

		it('can create a topic if non exist', () => {
			let topic = rxBus.assertTopic({name: topicName});
			should.exist(topic); 
			topic.should.be.a('object');
			topic.should.have.a.property('topic');
			rxBus.topics.should.have.property('length', 1);
			rxBus.topics.length.should.be.above(0);
		});

		it('should get the topic if exists', () => {
			let topic = rxBus.assertTopic({name: topicName});
			should.exist(topic);
			topic.should.be.an('object');
			topic.id.should.be.equal(1);
		});

		it('should return a list of topics id-name', () => {
			rxBus.topics.should.have.lengthOf(1);
			rxBus.topics.forEach(t => {
				t.id.should.be.a('number');
				t.name.should.be.a('string');
			});
		});
	})


	describe('publish', () => {
		let rxBus, topicName, topicNames;
		beforeEach(() => {
			rxBus = require('../src');
			topicName = 'topicOne';
			topicNames = ['t1', 't2', 't3'];
		});

		afterEach(() => {
			rxBus.topics.length = 0;
		});

		it('should get a topic to publish', () => {
			let topic = rxBus.assertTopic({name: topicName});
			topic.should.be.an('object');
			topic.should.have.property('id', 1);
			topic.should.have.property('topic');
		});

		it('should publish a message to a topic', (done) => {
			// Check amqplib publish/consume, actually it would be rxBus.publish(topic, data)
			// rxBus,subscribe(topic, handler)
			const data = {value: 'OK'};
			const fakeTopic = {
				id: 1,
				name: topicName,
				topic: {
					emit: () => {}
				}
			};

			// const topic = rxBus.assertTopic({name: topicName});
			const topicStub = sinon.stub(rxBus, '_findTopic').returns(fakeTopic);
			const stubEmitter = sinon.stub(fakeTopic.topic, 'emit')
				//.withArgs(topicName, data)
				.returns(true);
			// topic.topic.once(topicName, spyHandler);
			let ok = rxBus.publish(fakeTopic, data);
			
			ok.should.be.true;
			topicStub.should.have.been.calledOnce;
			stubEmitter.should.have.been.calledOnce;
			stubEmitter.should.have.been.calledWith(topicName, data);

			topicStub.restore();
			// stubEmitter.restore();
			done();
		});

		it('should publish multiple messages to topics', () => {
			const fakeTopics = [1, 2, 3].map(i => {
				return {id: i, name: 't'+i, topic: {emit: () => {}}	}
			});
			const data = [1, 2, 3].map(i => {value: i});
			const assertTopicStub = sinon.stub(rxBus, '_findTopic');
			fakeTopics.forEach(f => assertTopicStub.withArgs(f).returns(f));
			const fakeTopicStubs = fakeTopics.map(f => sinon.stub(f.topic, 'emit').returns(true));
			const oks = data.map((d, i) => rxBus.publish(fakeTopics[i], d));

			oks.forEach(o => o.should.be.true);
			data.forEach((d, i) => {
				fakeTopicStubs[i].should.have.been.calledOnce;
			});
			assertTopicStub.restore();
			fakeTopicStubs.forEach(f => f.restore());
		});

		xit('should publish multiple messages to topics', (done) => {
			const data = [1, 2, 3];
			const spies = [sinon.spy(), sinon.spy(), sinon.spy()];
			spies.forEach(s => should.exist(s));
			const topics = topicNames.map(n => rxBus.assertTopic({name: n}));
			topics.forEach((t, i) => topics[i].topic.on(topics[i].name, spies[i]));
			const oks = topics.map((t, i) => rxBus.publish(t, data[i]));

			oks.forEach(o => o.should.be.true);
			
			data.forEach(i => {
				const spyHandler = spies[i-1];
				should.exist(spyHandler);
				spyHandler.should.have.been.calledWith(i);
				spyHandler.should.have.been.calledOnce;
			});
			done();
		});

		xit('should publish random events', (done) => {
			const spyConsumers = [sinon.spy(), sinon.spy()];
			const name = 't1'; // reusing the previous topic
			const nameParam = {name};
			const topic = rxBus.assertTopic(nameParam);
			should.exist(topic);

			spyConsumers.forEach(s => topic.topic.on(name, s));
			let cont = 1;
			// publising events to bus randomly timed n times
			let intervalId = setInterval(() => {
				rxBus.publish(topic, cont++);
				spyConsumers[0].should.have.been.calledWith(cont-1);
				spyConsumers[1].should.have.been.calledWith(cont-1);

				if (cont > 3) {
					spyConsumers[0].callCount.should.be.equal(3);
					clearInterval(intervalId);
					done();
				}
			}, 250);
		});
	});


	describe('subscribe', () => {
		let rxBus, tName, spyHandlers;
		beforeEach(() => {
			rxBus = require('../src');
			tName = 't1';
			spyHandlers = [sinon.spy(), sinon.spy()];
		});

		afterEach(() => {
			rxBus.topics.length = 0;
			spyHandlers.length = 0;
		});

		it('should consume a message from the subscribed topic', () => {
			const topic = rxBus.assertTopic({name: tName});
			const spyHandler = spyHandlers[0];
			const consumer = rxBus.consume(topic, spyHandler);
			const data = {value: true};
			rxBus.publish(topic, data);

			spyHandler.should.have.been.calledOnce;
		});

		it('should consume messages in order', (done) => {
			const topic = rxBus.assertTopic({name: tName});
			// const firstSpy = sinon.spy(), sndSpy = sinon.spy();
			// rxBus.consume(topic, sndSpy);
			// rxBus.consume(topic, firstSpy);
			const consumers = spyHandlers.forEach(c => rxBus.consume(topic, c)); 
			const data = [1, 2, 3];
			let i = 0;
			const intervalId = setInterval(() => {
				rxBus.publish(topic, data[i++]);
				
				if (i === 3) {
					const calls0 = spyHandlers[0].getCalls(), calls1 = spyHandlers[1].getCalls();
					const params = calls0.map(call => call.args);
					spyHandlers[0].should.have.been.calledThrice;
					spyHandlers[1].should.have.been.calledThrice;

					spyHandlers[0].firstCall.args[0].should.be.deep.equal(1);
					data.forEach((c, i) => spyHandlers[0].getCall(i).args[0].should.be.equal(c));
					data.forEach((c, i) => spyHandlers[1].getCall(i).args[0].should.be.equal(c));

					clearInterval(intervalId);
					done();
				}
			}, 250);
		});
	});
});